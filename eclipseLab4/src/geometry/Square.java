package geometry;

public class Square extends Rectangle {
//Extends from Rectangle , therefore a square automatically 
//has the same properties of the rectangle class
	
//for a shape to qualify as a square, all of its sides have to be the same length	
	public Square (double sideLength) {
		super(sideLength,sideLength); //Calling the Rectangle constructor.
	}
}
