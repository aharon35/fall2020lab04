package geometry;

public class Circle implements Shape {
	
	private double radius;
	
	//Creates a circle and assigns
	public Circle(double radius) {
		this.radius = radius;
	}
	//Retrieves the radius of the circle
	public double getRadius() {
		return this.radius;
	}
	
	@Override
	//Retrieves the area of the circle 
	public double getArea() {
		double area = (Math.PI * (this.radius * this.radius));//the formula to get the area of a circle
		return area;
	}

	@Override
	//Retrieves the circumference of the circle
	public double getPerimeter() {
		double perimeter = 2 * (Math.PI * this.radius); //The formula to get the perimeter/circumference of a circle
		return perimeter;
	}

}
