package geometry;

public class Rectangle implements Shape {
	private double length;
	private double width;
	
	//Creates the rectangle and assigns its given parameters to it
	public Rectangle(double length,double width) { 
		this.length = length;
		this.width = width;
	}
	//Retrieves the length value of the Rectangle
	public double getLength() {
		return this.length;
	}
	//Retrieves the width value of the Rectangle
	public double getWidth() {
		return this.width;
	}
	
	@Override
	//Retrieves the Area of a rectangle
	public double getArea() {
		double area = this.length * this.width; //formula to get the area of a rectangle
		return area;
	}

	@Override
	//Retrieves the perimeter of a rectangle
	public double getPerimeter() {
		double perimeter = 2*(this.length+this.width); //formula to get the perimeter of a rectangle
		return perimeter;
	}

}
