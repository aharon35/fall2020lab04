package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
	   Shape[] array = new Shape[5];
	   array[0] = new Rectangle(5.0,10.0); //perimeter 30 , area 50
	   array[1] = new Rectangle(2.0,4.0); //perimeter 12 , area 8
	   array[2] = new Circle(8.0);//circumference 50.26548245743669 , area 201.06192982974676
	   array[3] = new Circle(16.0);//circumference 100.53096491487338 , area 804.247719318987
	   array[4] = new Square(6.0);//perimeter 24.0, area 36.0
	   
	   for(int i = 0; i<array.length; i++) {
		   if(i==0 || i==1) {
			   System.out.println("Rectangle" + (i + 1) +", Perimeter: " + array[i].getPerimeter() + "\n");
			   System.out.println("Rectangle" + (i + 1) +", Area: " + array[i].getArea() + "\n");
		   }else  if(i==2 || i==3) {
			   System.out.println("Circle" + (i + 1) +", Circumference: " + array[i].getPerimeter() + "\n");
			   System.out.println("Circle" + (i + 1) +", Area: " + array[i].getArea() + "\n");
		   }else {
			   System.out.println("Square" + (i + 1) +", Perimeter: " + array[i].getPerimeter() + "\n");
			   System.out.println("Square" + (i + 1) +", Area: " + array[i].getArea() + "\n");
			   
		   }
		   
	   }

	}

}
