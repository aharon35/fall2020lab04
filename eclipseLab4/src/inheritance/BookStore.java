package inheritance;

public class BookStore {
	 
	public static void main(String[] args) {
      
		Book[] books = new Book[5];
		books[0] = new Book("Harry Potter", "JK ROWLING");
		books[1] = new ElectronicBook("Tristar", "Firas Zahabi",4096);
		books[2] = new Book("Half Guard", "John Danaher");
		books[3] = new ElectronicBook("Mount Control", "Gordon Ryan", 93012);
		books[4] = new ElectronicBook("Takedown Defense 101", "Henry Cejudo" , 333333);
		
		for(int i=0; i<books.length; i++) {
			System.out.println(books[i].toString());
		}
		
		
	}

}
