package inheritance;

public class Book {
	protected String title;
	private String authorName;
	
	public String getTitle() {
		return this.title;
	}

	public String getAuthorName() {
		return this.authorName;
	}
	//Creates a Book object and assigns its parameters given to it
	public Book (String title , String aName) {
		this.title = title;
		this.authorName = aName;
	}
	
	//Calling this method will convert the Book object into a String representation
	public String toString() {
		return "Title: " + this.title + "\n" + "Name of the Author: " + this.authorName + "\n" ;
		
	}
	
	
}

