package inheritance;

public class ElectronicBook extends Book {
 private int numberBytes;
 
 //Extends from Book , therefore a ElectronicBook automatically 
//has the same properties of the Book class
 public ElectronicBook(String title,String aName,int numberBytes) {
	super(title,aName); //This calls the constructor from the book class
	this.numberBytes = numberBytes;
 }
 
 public String toString() {
	 String fromBook = super.toString();
	 return fromBook + "Number of Bytes: " + this.numberBytes + "\n";
 }
 
}
